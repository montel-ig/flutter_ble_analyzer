import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:flutter_blue/flutter_blue.dart';

var logger = Logger();
FlutterBlue flutterBlue = FlutterBlue.instance;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  BluetoothDevice? _connectedDevice;
  bool _listeningOverBluetooth = false;

  @override
  void initState() {
    super.initState();
  }

  /// Bluetooth connections start by looking for advertising bluetooth devices.
  /// Often you might store the MAC-address of the device you connect to so
  /// that you don't need to scan every time you start your app.
  /// Because this is a simple demo we don't do that.
  void _startScanAndConnect() {
    // start a scan and listen for results
    flutterBlue.startScan(timeout: const Duration(seconds: 5));

    // Let's store the unique results in a simple array
    var scanResults = [];
    flutterBlue.scanResults.listen((results) {
      for (ScanResult r in results) {
        if (!scanResults
            .map((found) => found.device.id)
            .contains(r.device.id)) {
          scanResults.add(r);
        }
      }
    });

    // After five seconds we pick the first "Kickai" named device we
    // find. Under normal conditions you would probably show the user
    // a list of matching devices and let them pick one. We don't.
    Future.delayed(const Duration(seconds: 5), () async {
      var list =
          scanResults.map((r) => "${r.device.id} (${r.device.name})").toList();
      logger.i(list);

      var kickAiDevices = scanResults.where((r) => r.device.name == 'Kickai');
      if (kickAiDevices.isNotEmpty) {
        BluetoothDevice device = kickAiDevices.first.device;
        try {
          logger.i("Connect to ${device.id} (${device.name})");
          await device.connect(timeout: const Duration(seconds: 10));
          // await device.connect();
        } catch (e) {
          logger.e("No success connecting");
          return;
        }
        List<BluetoothService> services = await device.discoverServices();
        Map<String, List<Map<String, String>>> humanReadableServices = {};
        for (var service in services) {
          var characteristics = service.characteristics;
          humanReadableServices[service.uuid.toString()] =
              <Map<String, String>>[];
          for (BluetoothCharacteristic c in characteristics) {
            Map<String, String> humanReadableCharacteristic = {
              "uuid": c.uuid.toString(),
              "serviceUuid": c.serviceUuid.toString(),
              "write": c.properties.write.toString(),
              "read": c.properties.read.toString()
            };
            if (c.properties.read) {
              List<int> value = await c.read();
              humanReadableCharacteristic["value"] = value.toString();
              humanReadableCharacteristic["stringValue"] =
                  String.fromCharCodes(value);
            }

            humanReadableServices[service.uuid.toString()]
                ?.add(humanReadableCharacteristic);
          }
        }
        logger.i(humanReadableServices);
        setState(() {
          _connectedDevice = device;
        });
      }
    });
  }

  /// Test some UART code with the device we hopefully found in the
  /// previous step
  void _playWithLed() async {
    // We know these beforehand because we've read the hardware manual.
    // These UUIDs help us find the services we are interested in
    const UART_SERVICE_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
    const UART_CHAR_RX_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
    const UART_CHAR_TX_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";

    // For demo purposes let's look for the receiving and transmitting
    // characteristics for the UART connection.
    //
    // This is a bit daft, ofc you would cache the values IRL.
    BluetoothCharacteristic? rxCharacteristic = null;
    BluetoothCharacteristic? txCharacteristic = null;
    var device = _connectedDevice!;
    List<BluetoothService> services = await device.discoverServices();
    for (var service in services) {
      if (service.uuid.toString() == UART_SERVICE_UUID) {
        var characteristics = service.characteristics;
        for (var characteristic in characteristics) {
          if (characteristic.uuid.toString() == UART_CHAR_RX_UUID) {
            rxCharacteristic = characteristic;
          } else if (characteristic.uuid.toString() == UART_CHAR_TX_UUID) {
            txCharacteristic = characteristic;
          }
        }
      }
    }

    // Let's start listening via notify and write some simple commands
    if (txCharacteristic != null && rxCharacteristic != null) {
      // listen for changes in the actual characteristic. This is also
      // something you'd setup somewhere else as a dedicated listener
      // method
      if (!_listeningOverBluetooth) {
        await txCharacteristic.setNotifyValue(true);
        txCharacteristic.value.listen((value) {
          print("TX got raw value: $value");
          logger.i(String.fromCharCodes(value));
        });
        setState(() {
          _listeningOverBluetooth = true;
        });
      }

      // turn all lights out
      var colors = ["R", "G", "B"];
      for (var c in colors) {
        await rxCharacteristic.write("led$c=0".codeUnits);
      }

      // turn on a random color
      var randColor = colors[Random().nextInt(colors.length)];
      var command = "led$randColor=1";
      await rxCharacteristic.write(command.codeUnits);

      // let's ask for the device version
      await rxCharacteristic.write("version".codeUnits);
    }
  }

  /// Your simple disconnect action. Really needed to make life easier
  /// when testing the connection code.
  void _disconnect() {
    _connectedDevice?.disconnect();
    setState(() {
      _connectedDevice = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              child: const Text('Scan and connect'),
              onPressed: _connectedDevice != null ? null : _startScanAndConnect,
            ),
            ElevatedButton(
              child: const Text('Play with LED'),
              onPressed: _connectedDevice == null ? null : _playWithLed,
            ),
            ElevatedButton(
              child: const Text('Disconnect'),
              onPressed: _connectedDevice == null ? null : _disconnect,
            ),
          ],
        ),
      ),
    );
  }
}
